"use strict";
document.addEventListener("DOMContentLoaded", setup);

/*create array to store projects*/
let all_projects_arr = new Array();

/*table*/
let table;

/*legend to update the user as to what happened*/
let textUpdate;

/*textbox to take query*/
let queryBox;

/**
 * Author: Manil Hammadouche
 */
function setup(){
    //table element
    table = document.getElementById("t1");
    
    //Status Bar
    textUpdate = createTagElement(table, "caption");

    //Add button
    const addButton = document.getElementById("add");
    addButton.addEventListener("click", updateProjectsTable);

    //Reset Button
    const resetButton = document.getElementById("reset");
    resetButton.addEventListener("click", resetFields);

    //Save/write to local storage button
    const saveButton = document.getElementById("write");
    saveButton.addEventListener("click", saveAllProjects2Storage);

    //Append to local storage Button
    const appendButton = document.getElementById("append");
    appendButton.addEventListener("click", appendAllProjects2Storage);

    //Clear local storage button
    const clearButton = document.getElementById("clear");
    clearButton.addEventListener("click", clearAllProjectsFromStorage);

    //Load from local storage button
    const loadButton = document.getElementById("load");
    loadButton.addEventListener("click", readAllProjectsFromStorage);

    //Filter input
    queryBox = document.getElementById("query");
    queryBox.addEventListener("change", filterProjects);

    //Make Sure the fields are empty when dom content loaded
    resetFields();

    //Data Validation Event Listeners
    
    const form1 = document.getElementById("PID");
    form1.addEventListener("input", enable_disable_button);
    const form2 = document.getElementById("Owner");
    form2.addEventListener("input", enable_disable_button);
    const form3 = document.getElementById("title");
    form3.addEventListener("input", enable_disable_button);
    const form4 = document.getElementById("hour");
    form4.addEventListener("input", enable_disable_button);
    const form5 = document.getElementById("rate");
    form5.addEventListener("input", enable_disable_button);
    const form6 = document.getElementById("desc");
    form6.addEventListener("input", enable_disable_button);
    const form7 = document.getElementById("cat");
    form7.addEventListener("input", enable_disable_button);
    const form8 = document.getElementById("stat");
    form8.addEventListener("input", enable_disable_button);

    //Add event listeners for sorting, each table header is a different field
    for (let i=1; i<8; i++){
        document.getElementById("sort"+i).addEventListener("click", function(){sortProjects(i-1)});
    }
    
}