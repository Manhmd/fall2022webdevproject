"use strict";

/**
 * Author: Manil Hammadouche
 * Returns a project object that it built from the inputs
 */
function createProjectObject(){
    let projectId = document.getElementById("PID").value;
    let ownerName = document.getElementById("Owner").value;
    let title = document.getElementById("title").value;

    //for category, need to match the letter to the correct value
    let categoryLetter = document.getElementById("cat").value;
    let category;
    if (categoryLetter == "b"){
        category = "Practical";
    }
    else if (categoryLetter == "c"){
        category = "Theoretical";
    }
    else if (categoryLetter == "d"){
        category = "Fundamental Research";
    }
    else if (categoryLetter == "e"){
        category = "Empirical";
    }
    let hours = document.getElementById("hour").value;
    let rate = document.getElementById("rate").value;

    //status is the same deal as category
    let statusLetter = document.getElementById("stat").value;
    let status;
    if (statusLetter == "b"){
        status = "Completed";
    }
    else if (statusLetter == "c"){
        status = "Ongoing";
    }
    else if (statusLetter == "d"){
        status = "Planned";
    }
    let description = document.getElementById("desc").value;
    
    return {"ProjectID":projectId, 
        "Owner":ownerName, 
        "Title":title, 
        "Category":category, 
        "Status":status, 
        "Hours":hours, 
        "Rate":rate, 
        "Description":description
    };
}

/**
 * Author: Manil Hammadouche
 * Adds a new project to the array and accordingly updates the html table, and updates the status bar
 */
function updateProjectsTable(){
    let new_project = createProjectObject();
    all_projects_arr.push(new_project);
    createTableFromArrayObjects(all_projects_arr);
    textUpdate.innerText = "Added 1 new project to current table";
}

/**
 * Author: Manil Hammadouche
 * Resets fields
 */
function resetFields(){
    document.getElementById("PID").value = "";
    document.getElementById("Owner").value = "";
    document.getElementById("title").value = "";
    document.getElementById("cat").value = "a";
    document.getElementById("hour").value = 0;
    document.getElementById("rate").value = 0;
    document.getElementById("stat").value = "a";
    document.getElementById("desc").value = "";
}

/**
 * Author: Manil Hammadouche
 * Ask for confirmation, and then deletes a project from the array and from the table
 */
function deleteTableRow(rowNumber){
    if (confirm("Are you REALLY ok with deleting " + Object.values(all_projects_arr[rowNumber])[0] + "???")){
        all_projects_arr.splice(rowNumber, 1);
        createTableFromArrayObjects(all_projects_arr);
        rowNumber++;
        textUpdate.innerText = "Removed project number "+ rowNumber;
    }
}

/**
 * Author: Manil Hammadouche
 * Transforms all the cells to inputs/selects that can be altered
 * Transforms the edit button into a save button which saves the data from the inputs
 * Adds event listeners for data validation to make sure the user can't enter invalid inputs
 */
function editTableRow(rowNumber){
    let cell;
    let form;
    let saveRowButton;
    let options = [];

    for (let i = 0; i < 8; i++){
        cell = table.rows[rowNumber+1].cells[i];
        cell.innerText = "";
        cell.setAttribute("id", "tableCell"+i);

        //The input boxes, aka all except categorey and status
        if (i != 3 && i != 4){
            form = createTagElement(cell, "input");
            form.setAttribute("id", "cell"+i);

            //number inputs for hours and rates
            if (i == 5 || i == 6){
                form.setAttribute("type", "number")
            }

            //make sure the default value is the same as one before
            form.setAttribute("value", Object.values(all_projects_arr[rowNumber])[i]);
        }   

        //category select
        if (i == 3){
            form = createTagElement(cell, "select");
            form.setAttribute("id", "cell"+i);
            options[0] = createTagElement(form, "option");
            options[0].setAttribute("value", "a"); 
            options[0].innerText = "-----------";
            options[1] = createTagElement(form, "option");
            options[1].setAttribute("value", "b"); 
            options[1].innerText = "Practical";
            options[2] = createTagElement(form, "option");
            options[2].setAttribute("value", "c"); 
            options[2].innerText = "Theoretical";
            options[3] = createTagElement(form, "option");
            options[3].setAttribute("value", "d"); 
            options[3].innerText = "Fundamental Research";
            options[4] = createTagElement(form, "option");
            options[4].setAttribute("value", "e"); 
            options[4].innerText = "Empirical";

            //make sure the default value is the same as one before
            if (Object.values(all_projects_arr[rowNumber])[i] == "Practical"){
                options[1].setAttribute("selected", "selected");
            }
            else if (Object.values(all_projects_arr[rowNumber])[i] == "Theoretical"){
                options[2].setAttribute("selected", "selected");
            }
            else if (Object.values(all_projects_arr[rowNumber])[i] == "Fundamental Research"){
                options[3].setAttribute("selected", "selected");
            }
            else if (Object.values(all_projects_arr[rowNumber])[i] == "Empirical"){
                options[4].setAttribute("selected", "selected");
            }
            else {
                options[0].setAttribute("selected", "selected");
            }
            
        }

        //Status Select
        if (i == 4){
            form = createTagElement(cell, "select");
            form.setAttribute("id", "cell"+i);
            options[0] = createTagElement(form, "option");
            options[0].setAttribute("value", "a"); 
            options[0].innerText = "-----------";
            options[1] = createTagElement(form, "option");
            options[1].setAttribute("value", "b"); 
            options[1].innerText = "Completed";
            options[2] = createTagElement(form, "option");
            options[2].setAttribute("value", "c"); 
            options[2].innerText = "Ongoing";
            options[3] = createTagElement(form, "option");
            options[3].setAttribute("value", "d"); 
            options[3].innerText = "Planned";

            //make sure the default value is the same as one before
            if (Object.values(all_projects_arr[rowNumber])[i] == "Completed"){
                options[1].setAttribute("selected", "selected");
            }
            else if (Object.values(all_projects_arr[rowNumber])[i] == "Ongoing"){
                options[2].setAttribute("selected", "selected");
            }
            else if (Object.values(all_projects_arr[rowNumber])[i] == "Planned"){
                options[3].setAttribute("selected", "selected");
            }
            else {
                options[0].setAttribute("selected", "selected");
            }
        }

        //creating the image that will become either the checkmark or error
        createTagElement(cell, "img");

        //adding the data validation
        form.addEventListener("input", enable_disable_save_button);
    }
    
    //save button
    cell = table.rows[rowNumber+1].cells[8];
    cell.innerText = "";
    saveRowButton = createTagElement(cell, "button");
    saveRowButton.setAttribute("id", "save");
    saveRowButton.innerText = "Save";
    saveRowButton.addEventListener("click", function(){saveRowEdit(rowNumber)});
    
    //Display the one that is being editted in status bar
    let tableNumber = rowNumber + 1;
    textUpdate.innerText = "Editing project number "+ tableNumber;
}

/**
 * Author: Manil Hammadouche
 * Function called when save button is clicked
 * Saves the values as the new values of the object in the array
 */
function saveRowEdit(rowNumber){

    //category
    let categoryLetter = document.getElementById("cell3").value;
    let category;
    if (categoryLetter == "b"){
        category = "Practical";
    }
    else if (categoryLetter == "c"){
        category = "Theoretical";
    }
    else if (categoryLetter == "d"){
        category = "Fundamental Research";
    }
    else if (categoryLetter == "e"){
        category = "Empirical";
    }

    //status
    let statusLetter = document.getElementById("cell4").value;
    let status;
    if (statusLetter == "b"){
        status = "Completed";
    }
    else if (statusLetter == "c"){
        status = "Ongoing";
    }
    else if (statusLetter == "d"){
        status = "Planned";
    }

    //saving into the array
    all_projects_arr[rowNumber] = {"ProjectID":document.getElementById("cell0").value, 
        "Owner":document.getElementById("cell1").value, 
        "Title":document.getElementById("cell2").value, 
        "Category":category, 
        "Status":status,
        "Hours":document.getElementById("cell5").value, 
        "Rate":document.getElementById("cell6").value, 
        "Description":document.getElementById("cell7").value
    };

    //recreate the table based on the updated array
    createTableFromArrayObjects(all_projects_arr);

    //Display the project that is was altered in status bar
    rowNumber++;
    textUpdate.innerText = "Changed project number "+ rowNumber;
}

/**
 * Author: Manil Hammadouche
 * Clears local storage
 * Uses a for loop to store each project in the table in local storage
 */
function saveAllProjects2Storage(){
    localStorage.clear();
    for (let i = 0; i < all_projects_arr.length; i++){
        localStorage.setItem("Project" + i, JSON.stringify(all_projects_arr[i]));
    }

    //update status bar
    textUpdate.innerText ="There are now "+ localStorage.length + " projects in local storage.";
}

/**
 * Author: Manil Hammadouche
 * Uses a for loop to store each project in the table at the end of local storage
 */
function appendAllProjects2Storage(){
    let numStoredProjects = localStorage.length;
    for (let i = 0; i < all_projects_arr.length; i++){
        localStorage.setItem("Project" + numStoredProjects, JSON.stringify(all_projects_arr[i]));
        numStoredProjects++;
    }

    //update status bar
    textUpdate.innerText ="Added "+ all_projects_arr.length +" projects to storage, there are now "+ localStorage.length + " projects in local storage.";
}

/**
 * Author: Manil Hammadouche
 * Clears local storage
 */
function clearAllProjectsFromStorage(){
    localStorage.clear();

    //Update status bar
    textUpdate.innerText ="Cleared local storage, there are now "+ localStorage.length +" projects in local storage.";
}

/**
 * Author: Manil Hammadouche
 * Display all projects from storage, storing them in the array and table
 */
function readAllProjectsFromStorage(){
    //start by emptying array
    all_projects_arr = [];

    //for loop to put each object into the array
    for (let i = 0; i < localStorage.length; i++){  
        all_projects_arr.push(JSON.parse(localStorage.getItem("Project"+i) || "[]"));
    }
    createTableFromArrayObjects(all_projects_arr);

    //update status bar
    textUpdate.innerText = "Reading "+ all_projects_arr.length +" projects from local storage";
}

/**
 * Manil Hammadouche
 * Filters through the objects that match the query
 */
function filterProjects(){
    let query = queryBox.value;
    let returnedArr = all_projects_arr.filter(obj => {
        for (let i =0; i < Object.values(obj).length; i++){
            if (Object.values(obj)[i].match(query)) {return true;}
        }
        return false;
    });

    //display the projects that match the query, not altering the array however
    createTableFromArrayObjects(returnedArr);

    //update status bar
    textUpdate.innerText = "There is/are " + returnedArr.length + " projects for your query";
}

/**
 * Author: Manil Hammadouche
 * Takes as input a number representing a field
 * Sorts in ascending order depending on which field is input
 */
function sortProjects(field){
    //The sort does not work if there is nothing in the array
    if (all_projects_arr.length == 0){
        alert("Nothing in the table, impossible to sort");
    }
    
    else {
        let sortedField = Object.keys(all_projects_arr[0])[field];

        //for hour and rate, sort like a number
        if (field == 5 || field == 6){
            all_projects_arr.sort(function(a, b) {return a[sortedField] - b[sortedField]});
        }

        //for other fields, sort like strings
        else {
            all_projects_arr.sort(function(a, b) {if(a[sortedField] < b[sortedField]){return -1} else{return 1}});
        }

        //update table to show sorted array
        createTableFromArrayObjects(all_projects_arr);
        
        //update status bar
        textUpdate.innerText = "Sorted Projects by "+ sortedField;
    }
 }