"use strict";

/**
 * Author: Daniel Giove
 * Calls validate element on each input with a regex
 * If all are true, add button is enabled
 * if any are false, add button is disabled
 */
function enable_disable_button(){
    let addBtn = document.getElementById("add");
    let validInputs =[];
    validInputs[0] = validateElement(document.getElementById("PID"), /^[a-zA-Z][\d\w\-\$]{2,9}$/);
    validInputs[1] = validateElement(document.getElementById("Owner"), /^[a-zA-Z][\d\w\-]{2,9}$/i);
    validInputs[2] = validateElement(document.getElementById("title"), /^[aA-zZ]{3,25}$/);
    validInputs[3] = validateElement(document.getElementById("hour"), /^(0|\d){1,3}$/);
    validInputs[4] = validateElement(document.getElementById("rate"), /^(0|\d){1,3}$/);
    validInputs[5] = validateElement(document.getElementById("desc"), /^[aA-zZ\s]{3,65}$/);
    validInputs[6] = validateElement(document.getElementById("cat"), /^[bcde]$/i);
    validInputs[7] = validateElement(document.getElementById("stat"), /^[bcd]$/i);


    try{
        validInputs.forEach(element => {
            if (element == false){
                throw "invalid input";
            }
        });
        addBtn.disabled = false;
    }catch(e){
        addBtn.disabled = true;
    }
}

/**
 * Author: Manil Hammadouche 
 * Same thing as enable_disable_button, but for the table when editting so the id's are different
 * disables the save button if any are false
 */
function enable_disable_save_button(){
    let saveBtn = document.getElementById("save");
    let validInputs =[];
    validInputs[0] = validateElement(document.getElementById("cell0"), /^[a-zA-Z][\d\w\-\$]{2,9}$/);
    validInputs[1] = validateElement(document.getElementById("cell1"), /^[a-zA-Z][\d\w\-]{2,9}$/i);
    validInputs[2] = validateElement(document.getElementById("cell2"), /^[aA-zZ]{3,25}$/);
    validInputs[3] = validateElement(document.getElementById("cell3"), /^[bcde]$/i);
    validInputs[4] = validateElement(document.getElementById("cell4"), /^[bcd]$/i);
    validInputs[5] = validateElement(document.getElementById("cell5"), /^(0|\d){1,3}$/);
    validInputs[6] = validateElement(document.getElementById("cell6"), /^(0|\d){1,3}$/);
    validInputs[7] = validateElement(document.getElementById("cell7"), /^[aA-zZ\s]{3,65}$/);


    try{
        validInputs.forEach(element => {
            if (element == false){
                throw "invalid input";
            }
        });
        saveBtn.disabled = false;
    }catch(e){
        saveBtn.disabled = true;
    }
}

/**
 * Author: Daniel Giove
 * Takes as input an element and a regex, and tests the element to see if it matches
 * calls setElementFeedback, then returns the boolean of whether the test was successful or not
 */
 function validateElement(elem, regex){
    setElementFeedback(regex.test(elem.value), elem);
    return regex.test(elem.value);
}

/**
 * Author: Daniel Giove
 * If the input is true, sets the image as the checkmark
 * If not, sets image as error
 */
function setElementFeedback(booleanValue, elem){
    if (booleanValue){
        document.querySelector(`#${elem.parentElement.getAttribute("id")} > img`).setAttribute("src", "../images/checkmark.png");
    }
    else {
        document.querySelector(`#${elem.parentElement.getAttribute("id")} > img`).setAttribute("src", "../images/error.png");
    }
}

/**
 * Author: Manil Hammadouche
 * Creates html from a project of arrays
 */
function createTableFromArrayObjects(projectArray){
    if (document.querySelector("tbody")){
        document.querySelector("tbody").remove();
    }

    let tableBody = createTagElement(table, "tbody");
    
    let row;
    let cell;
    let editButton;
    let deleteButton;

    //for loop for number of rows
    for (let i = 0; i < projectArray.length; i++){
        row = createTagElement(tableBody, "tr");

        //for loop for number of cells, 1 for each field, taking the value from the object to fill 
        for (let values of Object.values(projectArray[i])){
            cell = createTagElement(row, "td");
            cell.innerText = values;
        }

        //cell for edit button, with event listener
        cell = createTagElement(row, "td");
        editButton = createTagElement(cell, "img");
        editButton.setAttribute("src", "../images/edit.png");
        editButton.addEventListener("click", function(){editTableRow(i)});

        //cell for delete button, with event listener
        cell = createTagElement(row, "td");
        deleteButton = createTagElement(cell, "img");
        deleteButton.setAttribute("src", "../images/delete.png");
        deleteButton.addEventListener("click", function(){deleteTableRow(i)});
    
    }
    
}

/**
 * Author: Manil Hammadouche
 * Takes as input a parent and a tag name
 * Creates tag element by doing appendChild on the parent with the tagname
 * returns created tag element
 */
function createTagElement(parent, tagName){
    let tagElem = document.createElement(tagName);
    parent.appendChild(tagElem);
    return tagElem;
}