# f22-proj1-grp4-HAMMADOUCHE

This website can do the following things:

Using the many fields, you can input the fields of a project object. You can press the reset button to empty all the felds. Once all the fields are valid, you may click the add button to create the project and add it to the array. You may see the array in the table.

Once a project is in a table, you can either edit it or delete it. 
If you press the delete button, the website will ask you to confirm if you really want to erase it or not. If you say no, nothing happens. If you say yes, the project is deleted.
If you press the edit button, the cells of the project become input fields that can be altered. They are still subject to data validation. Once all the fields are valid, you may click the save button that has now replaced the edit button, which saves your edits in the array, altering the project you were editting.

We then have the 4 local storage functions:
-Write(save) will clear the current local storage and save the current array of projects into local storage
-Append adds the array of projects to the end of local storage
-Clear clears local storage
-Read will display the contents of the local storage as the current array and table

Filter will allow you to input a query, with the table then displaying any projects that match the query

By clicking on any of the table headers other than description, edit, and delete, you may sort the table in ascending order by that field.